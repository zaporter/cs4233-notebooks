{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8957d27d-0e39-4cae-b724-10de18eda761",
   "metadata": {},
   "source": [
    "# Streams: Introduction\n",
    "\n",
    "Streams have become common features of many languages/libraries for processing large amounts of data (or even smaller datasets) using a declarative programming approach. What is a stream? This is a pretty good definition:\n",
    "\n",
    ">A Stream is a sequence of elements supporting sequential and parallel aggregate operations.\n",
    "\n",
    "Many students are probably familiar somewhat with [MapReduce](https://en.wikipedia.org/wiki/MapReduce); if you are, then streams will seem quite natural. This notebook will give you a taste of streams in Java. Understanding the basics of how to effectively understand and use streams is now expected of Java programmers in industry today.\n",
    "\n",
    "## First stream example\n",
    "\n",
    "Java supports many types of streams. Many, if not most, of the Java collections contain some type of stream support. Stream utilities abound with a growing number of functions that one can chain together to perform very complex processing efficiently and succinctly. Here is a simple example of computing the sum of a sequence of integers:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a97e2a3e-0e8a-4648-a535-83a3e700f6c7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import java.util.stream.IntStream;\n",
    "\n",
    "IntStream is = IntStream.range(1, 10);\n",
    "System.out.println(is.sum());"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd71e433-c014-45ef-a26e-d90729acc547",
   "metadata": {},
   "source": [
    "Is the result what you expected? If not, why not? When using new, or seldom used methods, it is a good practice to look at the API documentation and to make sure that you have tests that ensure that the result you expect is the result you get. Modify the code above to print out each element of the stream, `is` rather than the sum of the integers in the stream.\n",
    "\n",
    "This short code snippet has a few thing worth noting.\n",
    "* `IntStream` is an interface. You can have implementations for default and static methods in an interface.\n",
    "* `range` is a static method that returns a sequence of integers in a range as a Stream.\n",
    "* `sum` is another method that obviously returns the sum of the integers in the stream. It is not static. In this case it is called upon the real instance of the IntStream, `is`.\n",
    "\n",
    "## Chaining functions\n",
    "\n",
    "One nice feature of streams is that methods can easily be chained together to perform complex calculations. What if we have a set of integers and only want to count how many are positive integers? We need to filter out those we don't want and then reduce the resulting set to the count. We do this succinctly and elegantly with the streams."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c3a4160-7d06-421e-8596-9fa25aab8a61",
   "metadata": {},
   "outputs": [],
   "source": [
    "import java.util.stream.IntStream;\n",
    "import java.util.Random;\n",
    "\n",
    "Random r = new Random();\n",
    "long positives = r.ints(5000).filter((i) -> i > 0).count();\n",
    "System.out.format(\"Out of 5000 random numbers, %d are positive%n\", positives );"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f39fe4b-9d95-480b-b3af-0411274f0ac6",
   "metadata": {},
   "source": [
    "In this example we create a stream of 5000 random numbers and then filter out the positive numbers using the `filter` method on the `IntStream`. The argument to this is a lambda `(i) -> i > 0` that is an instance of an [`IntPredicate`](https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/function/IntPredicate.html). This produces a stream that gets reduced by the `count` method.\n",
    "\n",
    "---\n",
    "\n",
    "![](../../images/Tryit.png)\n",
    "Modify the code above and try different predicates and get a feel for using chained methods with streams.\n",
    "\n",
    "The cell below is an empty Java cell. Use it to try some of the following stream exercises:\n",
    "1. Here is the way you can turn a collection into a stream, using `List` as an example:\n",
    "\n",
    "```\n",
    "List<String> myList =\n",
    "    Arrays.asList(\"a2\", \"a1\", \"b1\", \"c2\", \"b1\", \"a2\", \"c2\", \"b1\");\n",
    "```\n",
    "\n",
    "    Turn the list into a stream and use the `Stream` API to sort the list and print out the sorted list, one string per line.\n",
    "    \n",
    "2. Use the same list of strings as above and use the `Stream` API to remove duplicates and print the results as in the previous example.\n",
    "\n",
    "3. Do these steps:\n",
    "    1. Create a stream from the file **wordlist.txt** in the current directory. This file contains over 4000 words, one word per line.\n",
    "    2. Count the number of words that have 'oo' in them.\n",
    "    3. Print the number of words found in the previous step.\n",
    "    \n",
    "4. Use the same input as in the previous exercise and count the number of words that end in 'nce' and print out the count.\n",
    "\n",
    "5. Using the same input, find all words that have 'ion' in them, sort them in *reverse alphabetic order* and print out the result, one word per line.\n",
    "\n",
    "---\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d03b7aa1-047e-4011-b3fd-b315016bbb1f",
   "metadata": {},
   "outputs": [],
   "source": [
    "// Use this cell for the above exercises\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7bd0dbb3-5fee-44e3-a0de-efceee4abd9c",
   "metadata": {},
   "source": [
    "## Stream operations and guidelines\n",
    "\n",
    "When you use streams some methods represent *terminal* operations that consume the stream, and others are *intermediate* operations that return the stream's contents after executing the method. The [Javadoc for the `Stream` interface](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html) says:\n",
    "> To perform a computation, stream operations are composed into a stream pipeline. A stream pipeline \n",
    ">consists of a source (which might be an array, a collection, a generator function, an I/O channel, etc), \n",
    "> zero or more intermediate operations (which transform a stream into another stream, such as \n",
    "> filter(Predicate)), and a terminal operation (which produces a result or side-effect, such as \n",
    "> count() or forEach(Consumer)). Streams are lazy; computation on the source data is only performed \n",
    "> when the terminal operation is initiated, and source elements are consumed only as needed.\n",
    "\n",
    "You must make sure that you end any stream pipeline with a terminal operation and have only intermediate operations in the pipeline before it. If you do not have the terminal operation and you do not close the stream your program will have memory leaks.\n",
    "\n",
    "There are several things that you must remember about using stream pipelines. Read the [overview Javadoc for the `Stream` interface](https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html) to learn about the constraints and conventions that you should follow.\n",
    "\n",
    "Previous: [A worked example final version evaluation](../SimpleExample/FirstExample-v3-evaluation.ipynb)  Next: [Java Generics Basics](../Generics/GenericsIntroduction.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b742f14a-04e9-4f86-ba22-99ca43c6dd35",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## References and further reading\n",
    "\n",
    "\\[1] *Introduction to Java 8 Streams*, Baeldung, https://www.baeldung.com/java-8-streams-introduction.\n",
    "\n",
    "[1]: https://www.baeldung.com/java-8-streams-introduction \"Introduction to Java 8 Streams\"\n",
    "\n",
    "\\[2]: *The Java 8 Stream API Tutorial*, Baeldung, https://www.baeldung.com/java-8-streams.\n",
    "\n",
    "[2]: https://www.baeldung.com/java-8-streams \"The Java 8 Stream API Tutorial\"\n",
    "\n",
    "\\[3] *Stream API*, Oracle, https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html.\n",
    "\n",
    "[3]: https://docs.oracle.com/javase/8/docs/api/java/util/stream/Stream.html \"Stream interface Javadoc\""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "Java",
   "pygments_lexer": "java",
   "version": "17+35-LTS-2724"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
