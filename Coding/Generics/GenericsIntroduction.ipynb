{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c5fd5e08-246e-410c-bc75-248ced261cae",
   "metadata": {},
   "source": [
    "# Generics: a powerful tool for your programming toolbox\n",
    "\n",
    "Many object-oriented programming languages support generics &mdash; a feature that allows you to parametize behavior that can operate on several, similar types. If you have not encountered generics in previous courses, you should go through the notebooks on generics carefully. If you are comfortable with generics then you should at least try the different exercises scattered throughout those notebooks.\n",
    "\n",
    "## A simple generic example\n",
    "\n",
    "What does it mean to parameterize a type. You probably have seen this already in some Java programs. Generics do not only apply to types, but to other elements like methods. Also, you are not limited to a single type parameter. Consider the following code snippet that defines a method to add an element to a list (yes, this is already a part of the `List` API, but it is a good, simple example:\n",
    "\n",
    "```\n",
    "public <T> void addToList(List<T> theList, T theItem) {\n",
    "    theList.add(theItem);\n",
    "}\n",
    "```\n",
    "\n",
    "Several things you should understand about this method:\n",
    "* It is parameterized with a type: `T`. This means that when the actual method is called `T` will be replaced by the type specified in the actual arguments.\n",
    "* `List<T>` is a parameterized list that only operates on single type. Almost all Java collections are parameterized with types. However, you do not have to parameterize them.\n",
    "* We use `List` instead of a particular type of list. This is a coding practice you should get used to. The idea is to *use the most inclusive type possible when declaring the type of an entity*. By using `List`, which is an interface rather than a specific type of list, such as `LinkedList`, the method applies to more cases.\n",
    "\n",
    "You do not have to paramterize lists or methods or anything. However, if you do not, you risk some client possibly passing the wrong type of object to your method and causing serious errors.\n",
    "\n",
    "The code cell below has the snippet above and some uses of it. It also has a non-genericized list that contains integers as well as strings. There are some commented out lines. What happens when you remove the comments? Explore this code and make sure that you understand the basic principles of generics. Try changing things, to see what happens with different cases.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "823ea03c-0978-49ac-88e9-39f409d97a49",
   "metadata": {
    "scrolled": true,
    "tags": []
   },
   "outputs": [],
   "source": [
    "public <T> void addToList(List<T> theList, T theItem) {\n",
    "    theList.add(theItem);\n",
    "}\n",
    "\n",
    "List<String> sList = new LinkedList<String>();\n",
    "addToList(sList, \"oops\");\n",
    "System.out.println(\"sList contains \" + sList.size() + \" items\");\n",
    "//addToList(lst, 1);\n",
    "//sList.add(1);\n",
    "\n",
    "List list1 = new LinkedList();\n",
    "addToList(list1, 0);\n",
    "list1.add(1);\n",
    "list1.add(\"F\");\n",
    "System.out.println(\"lst1 contains \" + list1.size() + \" items\");\n",
    "//System.out.println(list1.getClass().getName());\n",
    "//System.out.println(list.getClass().getName());"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b93624c7-49b4-4a33-84a7-a530642d701b",
   "metadata": {},
   "source": [
    "## Why use generics?\n",
    "\n",
    "The following list is taken from [2]. It is a comprehensive list of reasons to know about and use generics.\n",
    "\n",
    "* Stronger type checks at compile time.\n",
    "* Fixing compile-time errors is easier than fixing runtime errors\n",
    "* Elimination of casts. Which in turn is quicker.\n",
    "* Enabling coders to implement generic solutions, which can be reused for multiple purposes.\n",
    "* Future proofed for the datatypes of tomorrow.\n",
    "\n",
    "Generics give you great power, without a lot of overhead. What happens when you declare a class or method like `addToList` above. We can put any object into the list. The only thing that the method enforces is that the list can only accept objects of type `T` and that the arguments to the method are compatible. How does Java do this for methods and classes? When the JVM (Java Virtual Machine) encounters an a method or class with a concrete type to replace the generic, it then invokes the just-in-time (JIT) compiler to compile the code fot that method or class \"on the fly.\" The compiler is very efficient and the time to compile generics is small and only done one time for the program execution\n",
    "\n",
    "Generics are not as simple as the above example. That example illustrates a basic use of generics. See [1, 3, and 4] for more complete examples. In the next couple of notebooks we will look at some examples of generics that you should find useful in the project for this course and other assignments.\n",
    "\n",
    "Previous: [Java Streams Basics](../Streams/Introduction.ipynb)  Next: [Bounded Generics](BoundedGenerics.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77adb35f-6e25-4fcd-955d-640e7ed6b70d",
   "metadata": {},
   "source": [
    "---\n",
    "\n",
    "## References and further reading\n",
    "\n",
    "\\[1] *Lesson: Generics (Updated)*, Oracle, https://docs.oracle.com/javase/tutorial/java/generics/index.html.\n",
    "\n",
    "[1]: https://docs.oracle.com/javase/tutorial/java/generics/index.html \"Lesson on Java Generics from Oracle\"\n",
    "\n",
    "\\[2] * Coding Concepts &mdash; Generics*, DEV Community, https://dev.to/designpuddle/coding-concepts---generics-34cf.\n",
    "\n",
    "[2]: https://dev.to/designpuddle/coding-concepts---generics-34cf \"Coding Concepts &mdash; Generics\"\n",
    "\n",
    "\\[3] *The basics of Generics*, Baeldung, https://www.baeldung.com/java-generics.\n",
    "\n",
    "[3]: https://www.baeldung.com/java-generics \"The Basics of Generics\"\n",
    "\n",
    "\\[4]: *Learn Java Generics*, TutorialsPoint, https://www.tutorialspoint.com/java_generics/index.htm.\n",
    "\n",
    "[4]: https://www.tutorialspoint.com/java_generics/index.htm \"Learn Java Generics\""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Java",
   "language": "java",
   "name": "java"
  },
  "language_info": {
   "codemirror_mode": "java",
   "file_extension": ".jshell",
   "mimetype": "text/x-java-source",
   "name": "Java",
   "pygments_lexer": "java",
   "version": "17+35-LTS-2724"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
